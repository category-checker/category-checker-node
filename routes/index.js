const express = require("express");
const router = express.Router();

const trackerController = require("./controllers/tracker");

router.get("/", (req, res, next) => res.send("Server working"));

router.post("/tracking-data", trackerController.checkWebsiteCategory);

module.exports = router;
