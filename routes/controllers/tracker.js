const axios = require("axios").default;

const pyUrl = `http://${process.env.FLASK_HOST || "localhost"}:5000`;

const checkWebsiteCategory = async (req, res, next) => {
  try {
    let domain = req.body.domain.slice(4);
    const result = await axios.get(`${pyUrl}/websites?url=${domain}`);
    res.send({ data: result.data.category, ok: true });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  checkWebsiteCategory,
};
